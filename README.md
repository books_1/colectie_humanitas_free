# colectie_humanitas_free

## Content

```
Aristofan - Trei comedii.pdf
Charles Pierre Baudelaire - Jurnale intime.pdf
Ethel Greening Pantazzi - Romania in lumini si umbre.pdf
Florin Piersic Junior - Opere cumplite Vol. 01.pdf
Florin Piersic Junior - Opere cumplite Vol. 02.pdf
Florin Piersic Junior - Romantic Porno.pdf
Francis Scott Key Fitzgerald - Blandetea noptii.pdf
Francis Scott Key Fitzgerald - Marele Gatsby.pdf
Franz Kafka - Scrisoare catre tata.pdf
Friedrich Nietzsche - Dincolo de bine si de rau.pdf
Friedrich Nietzsche - Ecce-homo.pdf
Gabriel Liiceanu - Scrisori catre fiul meu.pdf
George Topirceanu - Memorii de razboi.pdf
Gilbert Keith Chesterton - Ereticii.pdf
Gottfried August Burger - Aventurile baronului Munchhausen.pdf
Iacob Negruzzi - Amintiri din Junimea.pdf
Immanuel Kant - Prolegomene.pdf
Ioana Parvulescu - Intoarcere in secolul 21.pdf
Ion Creanga - Amintiri din copilarie.pdf
Ion Ghica - Scrisori catre Vasile Alecsandri.pdf
Ion Luca Caragiale - Momente.pdf
James Augustine Aloysius Joyce - Oameni din Dublin.pdf
James Augustine Aloysius Joyce - Portret al artistului.pdf
James Oscar Noyes - Romania tara de hotar.pdf
James William Ozanne - Trei ani in Romania.pdf
Johann Wolfgang von Goethe - Faust.pdf
John Stuart Mill - Despre libertate.pdf
Kido Okamoto - Fiica negustorului de sake.pdf
Lewis Carroll - Alice in Tara Minunilor.pdf
Mateiu Ion Caragiale - Sub pecetea tainei.pdf
Maude Rea Parkinson - Douazeci de ani in Romania.pdf
Max Blecher - Inimi cicatrizate.pdf
Mihai Eminescu - Versuri din manuscrise.pdf
Monica Brosteanu & Francisca Baltaceanu - Cele mai frumoase pagini de intelepciune biblica.pdf
Nikolai Leskov - Pelerinul vrajit.pdf
Oscar Wilde - De profundis.pdf
Oscar Wilde - Portretul lui Dorian Gray.pdf
Panait Istrati - Chira Chiralina.pdf
Patrick O'Brien - Jurnalul unei calatorii.pdf
Paul Lindenberg - Regele Carol I.pdf
Platon - Banchetul sau Despre Iubire.pdf
Platon - Phaidros sau Despre frumos.pdf
Platon - Theaitetos.pdf
Radu Paraschivescu - Bazar bizar.pdf
Radu Paraschivescu - Romania in 7 gesturi.pdf
Radu Rosetti - Amintiri.pdf
Radu Rosetti - Parintele Zosim.pdf
Regina Maria - Tara pe care o iubesc.pdf
Richard Kunisch - Bucuresti si Stambul.pdf
Robert Musil - Omul fara insusiri.pdf
Robin George Collingwood - Eseu despre metoda filozofica.pdf
Sabina Cantacuzino - Din viata familiei I.C. Bratianu.pdf
Sfantul Augustin - Confesiuni.pdf
Sfantul Augustin - Despre minciuna.pdf
Stefan Zweig - Lumea de ieri. Amintirile unui european.pdf
Virginia Woolf - Doamna Dalloway.pdf
Virginia Woolf - Orlando.pdf
Virginia Woolf - Valurile.pdf
William Butler Yeats - Rosa Alchemica.pdf
```